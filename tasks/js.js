import config from './../webpack.config.babel.js';

export function webpack(gulp, plugins, notification) {
  return () =>
    plugins.webpackStream(config)
    .pipe(gulp.dest(config.output.path))
    .pipe(plugins.notify({
      message: notification,
      onLast: true
    }));
}
