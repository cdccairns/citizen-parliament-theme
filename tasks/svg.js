export function minify(gulp, plugins, glob, destination, notification) {
  return () =>
    gulp.src(glob)
    .pipe(plugins.svgo())
    .pipe(gulp.dest(destination))
    .pipe(plugins.notify({
      message: notification,
      onLast: true
    }));
}
