export function optimise(gulp, plugins, glob, destination, notification) {
  return () =>
    gulp.src(glob)
    .pipe(plugins.imagemin({
      progressive: true
    }))
    .pipe(gulp.dest(destination))
    .pipe(plugins.notify({
      message: notification,
      onLast: true
    }));
}

export function cwebp(gulp, plugins, glob, destination, notification) {
  return () =>
    gulp.src(glob)
    .pipe(plugins.webp({
      quality: 90,
      method: 6,
      sns: 100
    }))
    .pipe(gulp.dest(destination))
    .pipe(plugins.notify({
      message: notification,
      onLast: true
    }));
}
