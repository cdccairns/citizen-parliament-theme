import * as browser         from './tasks/browser.js';
import { default as build } from './tasks/build.js';
import { default as clean } from './tasks/clean.js';
import { default as copy  } from './tasks/copy.js';
import * as css             from './tasks/css.js';
import * as img             from './tasks/img.js';
import * as js              from './tasks/js.js';
import * as svg             from './tasks/svg.js';

export { browser };
export { build };
export { clean };
export { copy };
export { css };
export { img };
export { js };
export { svg };
