(function() {

  /**
   * Toggle the visibility of a given element and set associated ARIA attribute values.
   * This function is intended to be used on elements with a unique toggle control
   * and which _do not_ necessitate focus trapping.
   */

  let toggles = document.querySelectorAll('[data-toggle]');

  toggles.forEach(function(toggle) {

    /**
     * Query the existence of the target element. If the element does not exist,
     * stop executing the function.
     */

    const target = document.querySelector(toggle.dataset.toggle);
    if (!target) { return; }

    toggle.addEventListener('click', function() {

      const state = 'is-visible';
      const visible = target.classList.contains(state);

      /**
       * Show the target element.
       */

      if (visible !== true) {
        target.classList.add(state);
        target.removeAttribute('aria-hidden');
        toggle.setAttribute('aria-expanded', true);
      }

      /**
       * Hide the target element.
       */

      else {
        target.classList.remove(state);
        target.setAttribute('aria-hidden', true);
        toggle.setAttribute('aria-expanded', false);
      }
    });
  });
})();
