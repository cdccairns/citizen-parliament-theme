'use strict';

const fractal = module.exports = require('@frctl/fractal').create();

/**
 * Use Nunjucks template engine adapter for components.
 *
 * @link https://github.com/frctl/fractal/tree/main/packages/nunjucks
 */

const nunjucks = require('@frctl/nunjucks')({
  paths: ['templates']
})

fractal.components.engine(nunjucks);
fractal.components.set('ext', '.nunj');


/**
 * Set directory paths.
 *
 * @link https://fractal.build/guide/components/configuration-reference.html#path
 * @link https://fractal.build/guide/documentation/configuration-reference.html#path
 * @link https://fractal.build/guide/web/configuration-reference.html#static-path
 */

fractal.components.set('path', `${__dirname}/templates`);
fractal.docs.set('path', `${__dirname}/pages`);
fractal.web.set('static.path', `${__dirname}/../../dist/assets`);


/**
 * Provide context data from JSON files.
 */

let filters = require('./data/filters.json');
let narrative = require('./data/narrative.json');
let nav = require('./data/nav.json');
let partners = require('./data/partners.json');
let questions = require('./data/questions.json');
let recipients = require('./data/recipients.json');

fractal.components.set('default.context', {
  filters,
  narrative,
  nav,
  partners,
  questions,
  recipients
});


/**
 * Set project labels.
 *
 * @link https://fractal.build/guide/project-settings.html#project-related-metadata
 * @link https://fractal.build/guide/components/configuration-reference.html#label
 * @link https://fractal.build/guide/components/configuration-reference.html#title
 */

const labels = {};
labels.project = 'citizenparliament.be';
labels.components = 'Templates';

fractal.set('project.title', labels.project);
fractal.components.set('label', labels.components);
fractal.components.set('title', labels.components);


/**
 * Disable default statuses.
 *
 * @link https://fractal.build/guide/core-concepts/statuses.html#default-statuses
 */

fractal.components.set('default.status', null);
fractal.docs.set('default.status', null);


/**
 * Configure theme.
 *
 * @link https://fractal.build/guide/web/default-theme.html#configuration
 */

const mandelbrot = require('@frctl/mandelbrot');
const theme = mandelbrot({
  nav: ['docs', 'components'],
  panels: ['html', 'view', 'notes'],
  skin: 'white'
});

fractal.web.theme(theme);


/**
 * Configure development server.
 *
 * @link https://fractal.build/guide/web/configuration-reference.html#server-sync
 */

fractal.web.set('server.sync', true);
fractal.web.set('server.syncOptions', {
  port: 4000,
  notify: false,
  open: true,
  ui: false
});
